package ma.ehei.gi.TVAannotation.facture;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:tva.properties")
public class Factures {
	//@Value("${id}")
	@Value("1")
	private int id;
	
	@Value("10")
	//@Value("${Qte}")
	private int Qte;
	
	//@Value("#{T(Double).parseDouble('${value}')}")
	@Value("200")
	private double Prix;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQte() {
		return Qte;
	}

	public void setQte(int qte) {
		Qte = qte;
	}

	public double getPrix() {
		return Prix;
	}

	public void setPrix(double prix) {
		Prix = prix;
	}
	
	public double getMontant() {
		return getPrix()*getQte();
	}
}

