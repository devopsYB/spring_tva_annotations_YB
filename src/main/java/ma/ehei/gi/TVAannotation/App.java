package ma.ehei.gi.TVAannotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ma.ehei.gi.TVAannotation.tva.*;
import ma.ehei.gi.TVAannotation.itva.*;

/**
 * Hello world!
 *
 */

public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext contexte = new ClassPathXmlApplicationContext("ApplicationContexte.xml"); // Dependencies injection
		
		System.out.println("Le Prix totae des marchandises s'eleve avec TVA s'eleve a :");
		ITva E = contexte.getBean("ES", ES.class);
		ITva G = contexte.getBean("ES", ES.class);
		System.out.println("Le Montant dans l'espagne est : " + E.MontantApresTVA()+" DH");


		ITva F = contexte.getBean("FR", FR.class);
		ITva H = contexte.getBean("FR", FR.class);
		System.out.println("Le Montant dans la France est : " + F.MontantApresTVA()+" DH");

		ITva Q = contexte.getBean("MA", MA.class);
		System.out.println("Le Montant dans Le Maroc est : " + Q.MontantApresTVA()+" DH");
		
		
		
		// Teste Beans
		if(E.equals(G)) System.out.println("Beans ES est identique : True  and HC => "+E.hashCode()+" == "+G.hashCode());
		else System.out.println("Beans ES est identique : False  and HC => "+E.hashCode()+" =/= "+G.hashCode());//Singleton

		if(F.equals(H)) System.out.println("Beans est FR identique : True and => HC "+F.hashCode()+" == "+H.hashCode());
		else System.out.println("Beans FR est identique : False and => HC "+F.hashCode()+" =/= "+H.hashCode());//Prototype
	}

}