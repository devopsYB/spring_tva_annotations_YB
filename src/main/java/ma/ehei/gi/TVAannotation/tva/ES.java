package ma.ehei.gi.TVAannotation.tva;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ma.ehei.gi.TVAannotation.facture.Factures;
import ma.ehei.gi.TVAannotation.itva.ITva;
@PropertySource("classpath:tva.properties")
@Component
@Scope("singleton")

public class ES extends Factures implements ITva {
	// @Autowired  //Need parametre
	@Value("${value}")
	public double MontantApresTVA() { 
		return getPrix()*getQte()*1.18;
	}
}