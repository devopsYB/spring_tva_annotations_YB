package ma.ehei.gi.TVAannotation.tva;

import org.springframework.stereotype.Component;

import ma.ehei.gi.TVAannotation.facture.Factures;
import ma.ehei.gi.TVAannotation.itva.ITva;

@Component
public class MA extends Factures implements ITva {
	// @Autowired  //Need parametre
	public double MontantApresTVA() { 
		return getPrix()*getQte()*1.20;
	}

}