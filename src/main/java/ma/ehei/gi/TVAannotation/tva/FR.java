package ma.ehei.gi.TVAannotation.tva;

import ma.ehei.gi.TVAannotation.facture.Factures;
import ma.ehei.gi.TVAannotation.itva.ITva;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class FR extends Factures implements ITva {
	// @Autowired  //Need parametre
	public double MontantApresTVA() { 
		return getPrix()*getQte()*1.19;
	}

}